package dl.com.restfulwebservices.user;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class UserDaoService {

    private static List<User> users = new ArrayList<>();
    private int userCounter=3;

    static {
        users.add(new User(1,"Florian",new Date()));
        users.add(new User(2,"Mario",new Date()));
        users.add(new User(3,"Carlos",new Date()));
    }

    public List<User> findAll(){
        return users;
    }

    public User save(User user){
        if(user.getId() == null){
            user.setId(++userCounter);
        }
        users.add(user);
        return user;
    }

    public User findOne(int id){
        for(User user : users){
            if(user.getId() == id) return user;
        }
        return null;
    }

    public User delete(int id){
        Iterator<User> userIterator = users.iterator();
        while (userIterator.hasNext()){
            User user = userIterator.next();
            if(user.getId() == id){
                users.remove(user);
                return user;
            }
        }
        return null;
    }
}
