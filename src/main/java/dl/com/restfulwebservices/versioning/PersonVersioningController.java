package dl.com.restfulwebservices.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersioningController {

    //Versioning on the path (URI versioning)
    @GetMapping(path = "/v1/person")
    public PersonV1 getPersonV1() {
        return new PersonV1("Carl");
    }

    @GetMapping(path = "/v2/person")
    public PersonV2 getPersonV2() {
        return new PersonV2(new Name("Carl", "Jones"));
    }

    //Version based on the request params
    @GetMapping(value = "/person/param", params = "version=1")
    public PersonV1 paramV1() {
        return new PersonV1("Carl");
    }

    @GetMapping(value = "/person/param", params = "version=2")
    public PersonV2 paramV2() {
        return new PersonV2(new Name("Carl", "Jones"));
    }

    //Version based on the header
    @GetMapping(value = "/person/header", headers = "X-API-VERSION=1")
    public PersonV1 headerV1() {
        return new PersonV1("Carl");
    }

    @GetMapping(value = "/person/header", headers = "X-API-VERSION=2")
    public PersonV2 headerV2() {
        return new PersonV2(new Name("Carl", "Jones"));
    }

    //Version based on the produces - Called Content Negociation or Accept versioning
    @GetMapping(value = "/person/produces", headers = "application/dl.company.app-v1+json")
    public PersonV1 producesV1() {
        return new PersonV1("Carl");
    }

    @GetMapping(value = "/person/produces", produces = "application/dl.company.app-v2+json")
    public PersonV2 producesV2() {
        return new PersonV2(new Name("Carl", "Jones"));
    }
}