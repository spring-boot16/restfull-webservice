package dl.com.restfulwebservices.filtering;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FilteringResource {

    @GetMapping(path = "/filtering")
    public FilteringBean retrieveOne() {
        return new FilteringBean("field1", "field2", "field3");
    }

    @GetMapping(path = "filtering/all")
    public List<FilteringBean> retrieveAll() {
        return new ArrayList<FilteringBean>() {{
            add(new FilteringBean("field1", "field2", "field3"));
            add(new FilteringBean("field1", "field2", "field3"));
        }};
    }
}
